﻿using UnityEngine;
using System.Collections;
using ServiceLocation;
using UnityEngine.UI;
using Members;


namespace Enemies {

	public class EnemySpawner : MonoBehaviour {

		[Header("Parameters:")]
		[SerializeField] bool generateGrayEnemies = true;
		[SerializeField] float spawnPeriod = 1.0f;
		[SerializeField] float spawnPeriodVelocity = 0.1f;
		[Header("Components:")]
		[SerializeField] Enemy enemyPrefab = null;
		[SerializeField] Image hint = null;

		private IEnumerator Start() {

			hint.enabled = false;
			while(true) {

				SpawnEnemy();
				yield return new WaitForSeconds(spawnPeriod);
			}
		}

		private void Update() {
			
			spawnPeriod = Mathf.Max(0, spawnPeriod - spawnPeriodVelocity * Time.deltaTime);
		}

		public void SpawnEnemy() {

			var enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
			int membersAmount = ServiceLocator.Instance.BandManager.MembersAmount;
			int startIndex = generateGrayEnemies ? -1 : 0;
			int typeIndex = Random.Range(startIndex, membersAmount);
			enemy.SetType(typeIndex);

			hint.color = typeIndex == -1 ? Color.gray : Member.typeColors[typeIndex];
			hint.enabled = true;
		}
	}

}