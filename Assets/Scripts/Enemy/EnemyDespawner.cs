﻿using UnityEngine;


namespace Enemies {

	public class EnemyDespawner : MonoBehaviour {

		private void OnTriggerEnter2D(Collider2D collision) {
			
			Destroy(collision.gameObject);
		}
	}
}