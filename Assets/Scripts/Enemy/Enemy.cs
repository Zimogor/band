﻿using UnityEngine;
using System;
using Members;


namespace Enemies {

	public class Enemy : MonoBehaviour {

		[Header("Components:")]
		[SerializeField] Transform scaler = null;
		[SerializeField] BoxCollider2D boxCollider = null;
		[SerializeField] MeshRenderer meshRenderer = null;

		private int typeIndex;

		public void SetType(int typeIndex) {

			this.typeIndex = typeIndex;
			if (typeIndex >= Member.typeColors.Length) throw new Exception("typeIndex too large: " + typeIndex);

			var scale = scaler.localScale;
			var size = boxCollider.size;
			var offset = boxCollider.offset;
			if (typeIndex == -1) {
				scale.y = 0.5f;
				size.y = 0.2f;
				meshRenderer.material.color = Color.gray;
			} else {
				scale.y = 1.0f;
				size.y = 0.9f;
				meshRenderer.material.color = Member.typeColors[typeIndex];
			}
			offset.y = size.y * 0.5f;
			scaler.localScale = scale;
			boxCollider.size = size;
			boxCollider.offset = offset;
		}

		private void OnTriggerEnter2D(Collider2D collision) {
			
			var member = collision.GetComponent<Member>();
			if (!member) return;
			if (typeIndex == -1 || typeIndex != member.TypeIndex)
				member.Damage();

			Destroy(gameObject);
		}
	}
}