﻿using UnityEngine;
using System.Collections;


namespace Members {

	public class Mover : MonoBehaviour {

		private Coroutine coroutine = null;

		public void MoveTo(float pos, float time) {

			if (coroutine != null) StopCoroutine(coroutine);
			coroutine = StartCoroutine(MoveCoroutine(pos, time));
		}

		private IEnumerator MoveCoroutine(float xPos, float time) {

			float speed = Mathf.Abs(transform.localPosition.x - xPos) / time;
			while(true) {

				var pos = transform.localPosition;
				if (pos.x > xPos)
					pos.x = Mathf.Max(pos.x - Time.deltaTime * speed, xPos);
				else if (pos.x < xPos)
					pos.x = Mathf.Min(pos.x + Time.deltaTime * speed, xPos);
				else break;
			
				transform.localPosition = pos;
				yield return null;
			}

			coroutine = null;
		}
	}
}