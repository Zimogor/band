﻿using UnityEngine;
using System.Collections;


public class Jumper : MonoBehaviour {

	private float? jumpPos;
	private float height;
	private float jumpTime;
	private Coroutine coroutine = null;

	public void Jump(float xPos, float height, float jumpTime) {

		this.height = height;
		this.jumpTime = jumpTime;
		jumpPos = xPos;
	}

	private void Update() {
		
		if (transform.position.x >= jumpPos && coroutine == null) {

			jumpPos = null;
			coroutine = StartCoroutine(JumpCoroutine());
		}
	}

	private IEnumerator JumpCoroutine() {

		float factor = 0.0f;
		while(true) {

			factor = Mathf.Min(factor + Time.deltaTime / jumpTime, 1.0f);

			var pos = transform.position;
			var f = factor * 2.0f - 1.0f;
			pos.y = (-f * f + 1.0f) * height;
			transform.position = pos;

			if (factor == 1.0f) break;
			yield return null;
		}

		coroutine = null;
	}
}
