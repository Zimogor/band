﻿using UnityEngine;
using System;
using ServiceLocation;


namespace Members {

	public class Member : MonoBehaviour {

		[SerializeField] int lives = 10;
		[SerializeField] Mover mover = null;
		[SerializeField] Jumper jumper = null;
		[SerializeField] TextMesh text = null;
		[SerializeField] MeshRenderer meshRenderer = null;

		public static readonly Color[] typeColors = new Color[] { Color.red, Color.green, Color.blue, Color.yellow, Color.magenta, Color.cyan };

		private void Awake() {
			
			Lives = lives;
		}

		public void MoveTo(float pos, float time) => mover.MoveTo(pos, time);
		public void Jump(float xPos, float height, float time) => jumper.Jump(xPos, height, time);

		private int _typeIndex;
		public int TypeIndex {
			get { return _typeIndex; }
			set {

				_typeIndex = value;
				if (_typeIndex >= typeColors.Length) throw new Exception("typeIndex too large: " + _typeIndex);
				meshRenderer.material.color = typeColors[_typeIndex];
			}
		}

		private int _lives;
		private int Lives {
			get { return _lives; }
			set {

				_lives = value;
				text.text = value.ToString();
			}
		}

		public void Damage() {

			Lives = Mathf.Max(Lives - 1, 0);
			if (Lives == 0)
				ServiceLocator.Instance.LevelManager.PlayerLost();
		}
	}
}