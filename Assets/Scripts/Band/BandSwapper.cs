﻿using UnityEngine;
using System.Collections.Generic;
using ServiceLocation;
using InputManagement;
using Members;


namespace BandManagement {

	public class BandSwapper : MonoBehaviour {

		[SerializeField] float swapTime = 0.3f;

		private List<Member> members;
		private float swapTimer;
		private SideDirection? pendDirection = null;
		private float memberDistance;

		private void Awake() {
		
			var inputManager = ServiceLocator.Instance.InputManager;
			inputManager.OnSideSwap += Swap;
		}

		public void SetMembers(List<Member> members, float memberDistance) {

			this.memberDistance = memberDistance;
			this.members = members;
		}

		private void Update() {

			var prevSwapTimer = swapTimer;
			swapTimer -= Time.deltaTime;
			if (pendDirection != null && prevSwapTimer > 0.0f && swapTimer <= 0.0f) {

				Swap(pendDirection.Value);
				pendDirection = null;
			}
		}

		private void Swap(SideDirection direction) {
			if (swapTimer >= 0.15f) return;
			if (swapTimer > 0.0f) {
				pendDirection = direction;
				return;
			}

			swapTimer = swapTime;
			Member member = null;
			switch (direction) {
				case SideDirection.Left:
					for (int i = 0; i < members.Count - 1; i ++)
						members[i].MoveTo((-i - 1) * memberDistance, swapTime);

					member = members[members.Count - 1];
					members.RemoveAt(members.Count - 1);
					member.MoveTo(0, swapTime);
					members.Insert(0, member);
					break;
				case SideDirection.Right:
					for (int i = 1; i < members.Count; i ++)
						members[i].MoveTo((-i + 1) * memberDistance, swapTime);
					member = members[0];
					members.RemoveAt(0);
					member.MoveTo(-memberDistance * members.Count, swapTime);
					members.Add(member);
					break;
			}
		}
	}

}