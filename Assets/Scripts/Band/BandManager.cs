﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Members;
using ServiceLocation;


namespace BandManagement {

	public class BandManager : MonoBehaviour {

		[Header("Parameters:")]
		[SerializeField] int runDistance = 100;
		[SerializeField] float memberDistance = 1.0f;
		[SerializeField] int memberAmount = 5;
		[Header("Components:")]
		[SerializeField] Member memberPrefab = null;
		[SerializeField] BandSwapper bandSwapper = null;
		[SerializeField] BandJumper bandJumper = null;
		[SerializeField] Text distanceText = null;

		private int prevDistance = int.MinValue;

		private void Awake() {
		
			float pos = 0.0f;
			var members = new List<Member>();
			for (int i = 0; i < memberAmount; i ++, pos -= memberDistance) {

				members.Add(Instantiate(memberPrefab, new Vector3(pos, 0, 0), Quaternion.identity, transform));
				members[i].TypeIndex = i;
			}
			bandSwapper.SetMembers(members, memberDistance);
			bandJumper.SetMembers(members);
		}

		private void Update() {
			
			var distance = Mathf.Min((int)transform.position.x, runDistance);
			if (distance == prevDistance) return;
			prevDistance = distance;
			distanceText.text = $"{distance} / {runDistance} m.";

			if (distance == runDistance)
				ServiceLocator.Instance.LevelManager.PlayerWin();
		}

		public int MembersAmount => memberAmount;
		public float MembersDist => memberDistance;
	}

}