﻿using UnityEngine;


namespace BandManagement {

	public class BandMover : MonoBehaviour {

		[SerializeField] float velocity = 2.0f;

		private void Update() {
			
			transform.Translate(new Vector3(velocity * Time.deltaTime, 0.0f, 0.0f));
		}
	}

}