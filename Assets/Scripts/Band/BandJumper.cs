﻿using UnityEngine;
using ServiceLocation;
using System.Collections.Generic;
using Members;


namespace BandManagement {

	public class BandJumper : MonoBehaviour {

		[SerializeField] float height = 1.0f;
		[SerializeField] float jumpTime = 0.5f;

		private List<Member> members;

		private void Awake() {

			var inputManager = ServiceLocator.Instance.InputManager;
			inputManager.OnUpSwap = Jump;
		}

		public void SetMembers(List<Member> members) {

			this.members = members;
		}

		private void Jump() {

			foreach (var member in members)
				member.Jump(members[0].transform.position.x, height, jumpTime);
		}
	}
}