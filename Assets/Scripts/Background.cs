﻿using UnityEngine;


public class Background : MonoBehaviour {

	[SerializeField] MeshRenderer meshRenderer = null;

	private void LateUpdate() {
		
		int tilesAmount = 20;
		meshRenderer.material.mainTextureScale = new Vector2(tilesAmount, 1);
		float tileSize = transform.localScale.x / tilesAmount;
		meshRenderer.material.mainTextureOffset = -new Vector2(transform.position.x / tileSize, 0.0f);
	}
}
