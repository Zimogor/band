﻿using UnityEngine;
using ServiceLocation;


public class CameraController : MonoBehaviour {

	[Header("Parameters:")]
	[SerializeField] float visibleWidth = 15.0f;
	[SerializeField] Vector3 clipPoint = new Vector3(0, 0, 3);
	[Header("Components:")]
	[SerializeField] Camera cam = null;

	private void Start() {
		
		UpdateConstraint();
	}

	private void LateUpdate() {
		
		UpdateConstraint();
	}

	private void UpdateConstraint() {

		cam.orthographicSize = visibleWidth / cam.aspect * 0.5f;
		var bandManager = ServiceLocator.Instance.BandManager;
		var dist = bandManager.MembersAmount * bandManager.MembersDist;
		var needTrackPoint = bandManager.transform.position - new Vector3(dist, 0, 0);
		var curTrackPoint = cam.ViewportToWorldPoint(clipPoint);
		var disp = needTrackPoint - curTrackPoint;
		transform.Translate(disp, Space.World);
	}
}
