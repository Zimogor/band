﻿using UnityEngine;
using System;


namespace InputManagement {

	public enum SideDirection { Right, Left }

	public class InputManager : MonoBehaviour {

		[SerializeField] float swipeDistance = 10.0f;

		public Action<SideDirection> OnSideSwap;
		public Action OnUpSwap;

		private Vector2? touchStartPos;

		private void Update() {
		
			if (Input.GetKeyDown(KeyCode.RightArrow)) OnSideSwap?.Invoke(SideDirection.Right);
			if (Input.GetKeyDown(KeyCode.LeftArrow)) OnSideSwap?.Invoke(SideDirection.Left);
			if (Input.GetKeyDown(KeyCode.UpArrow)) OnUpSwap?.Invoke();

			if (Input.touches.Length > 0) {

				var touch = Input.GetTouch(0);
				switch (touch.phase) {

					case TouchPhase.Began:
						touchStartPos = touch.position;
						break;
					case TouchPhase.Canceled:
					case TouchPhase.Ended:
						touchStartPos = null;
						break;
					case TouchPhase.Moved:
						var direction = touch.position - touchStartPos;
						if (direction?.magnitude >= swipeDistance) {
							Swipe(direction.Value);
							touchStartPos = null;
						}
						break;
				}
			}
		}

		private void Swipe(Vector2 direction) {

			if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) {
				OnSideSwap?.Invoke(direction.x > 0 ? SideDirection.Right : SideDirection.Left);
			} else {
				if (direction.y > 0) OnUpSwap?.Invoke();
			}
		}
	}

}