﻿using UnityEngine;
using InputManagement;
using UnityEngine.Assertions;
using BandManagement;


namespace ServiceLocation {

	public class ServiceLocator : MonoBehaviour {

		public static ServiceLocator Instance { get; private set; }

		[SerializeField] LevelManager levelManager = null;
		[SerializeField] InputManager inputManager = null;
		[SerializeField] BandManager bandManager = null;

		private void Awake() {
			Assert.IsNull(Instance);
		
			Instance = this;
		}

		public InputManager InputManager => inputManager;
		public BandManager BandManager => bandManager;
		public LevelManager LevelManager => levelManager;
	}

}