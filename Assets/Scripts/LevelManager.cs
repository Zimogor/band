﻿using UnityEngine;
using UnityEngine.SceneManagement;


// порядок скриптов
// - ServiceLocator - управление сервисами
// - InputManager - управление вводом

public class LevelManager : MonoBehaviour {

	public void PlayerLost() {

		SceneManager.LoadScene(0);
	}

	public void PlayerWin() {

		SceneManager.LoadScene(0);
	}
}
