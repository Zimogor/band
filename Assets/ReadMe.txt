Скорость движения юнитов: Scene/Band/BandMover/Velocity
Количество юнитов: Scene/Band/BandManager/MemberAmount
Период появления юнитов: Scene/Band/EnemySpawner/EnemySpawner/SpawnPeriod
Скорость уменьшения периода спауна юнитов: Scene/Band/EnemySpawner/EnemySpawner/SpawnPeriodVelocity
Счётчик метров: Scene/Band/BandManager/RundDistance
Количество жизней: Assets/Prefabs/Member/Member/Lives
Подсказка: Scene/Canvas/Hint/Activate(Deactivate)
Серые враги: Scene/Band/EnemySpawner/EnemySpawner/GenerateGrayEnemies
Ширина видимости: Scene/MainCamera/CameraController/VisibleWidth
Угол, в котором рисуется крайний персонаж: Scene/MainCamera/CameraController/ClipPoint
Чувствительность свайпа: Scene/LevelManager/InputManager/SwipeDistance
